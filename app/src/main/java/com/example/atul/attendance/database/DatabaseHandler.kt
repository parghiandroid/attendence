package com.example.atul.attendance.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.atul.attendance.model.Data
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class DatabaseHandler(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    // region get all data
    val attendence: List<Data>
        get() {
            var data : MutableList<Data> = mutableListOf()
            val selectQuery = "SELECT * FROM " + Table.TABLE_ATTENDENCE

            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)
            if (cursor.moveToFirst()) {
                do {
                    data.add(Data(cursor.getString(1),cursor.getString(2)))
                    /*val jsonObject = JSONObject()
                    try {
                        jsonObject.put("user_identity", cursor.getString(1))
                        jsonObject.put("datetime", cursor.getString(2))
                        jsonArray.put(jsonObject)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }*/
                } while (cursor.moveToNext())
            }
            return data
        }
    //endregion

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_ATTANDENCE)
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Table.TABLE_ATTENDENCE)
        onCreate(sqLiteDatabase)
    }

    //region insert data
    fun Insertdata(qr_result: String, date_time: String) {
        val db = this.writableDatabase
        val mValues = ContentValues()
        mValues.put(Table.QR_RESULT, qr_result)
        mValues.put(Table.DATE_TIME, date_time)
        db.insert(Table.TABLE_ATTENDENCE, null, mValues)
        db.close()
    }
    //endregion

    //region For delete table
    fun Deletedata() {
        val db = this.writableDatabase
        db.execSQL("delete from " + Table.TABLE_ATTENDENCE)
        db.close()
    }
    //endregion

    companion object {

        //Database version
        private val DATABASE_VERSION = 1
        //Database name
        private val DATABASE_NAME = "attandence_db"

        private val CREATE_TABLE_ATTANDENCE = ("CREATE TABLE " + Table.TABLE_ATTENDENCE
                + "(" + Table.ID + " INTEGER PRIMARY KEY,"
                + Table.QR_RESULT + " TEXT,"
                + Table.DATE_TIME + " TEXT" + ")")
    }

}