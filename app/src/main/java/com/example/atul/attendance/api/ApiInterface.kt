package com.example.atul.retrofit.api

import com.example.atul.attendance.model.BodyParam
import com.example.atul.attendance.model.BodyParamsenddata
import com.example.atul.attendance.model.GETUSERDATA
import com.example.atul.attendance.model.response
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiInterface {

    @POST("process.php")
    fun savetime(@Header("Content-Type") Content_Type : String,
                 @Header("app-id") App_Id : String,
                 @Header("app-secret") app_secret : String,
                 @Header("device-id") device_id : String,
                 @Header("auth-token") auth_token : String,
                 @Body body : BodyParam): Call<response>

    @POST("process.php")
    fun senddata(@Header("Content-Type") Content_Type : String,
                 @Header("app-id") App_Id : String,
                 @Header("app-secret") app_secret : String,
                 @Header("device-id") device_id : String,
                 @Header("auth-token") auth_token : String,
                 @Body body : BodyParamsenddata): Call<response>

    @POST("process.php")
    fun getuserdata(@Header("Content-Type") Content_Type : String,
                 @Header("app-id") App_Id : String,
                 @Header("app-secret") app_secret : String,
                 @Header("device-id") device_id : String,
                 @Header("auth-token") auth_token : String,
                 @Body body : GETUSERDATA): Call<response>
}