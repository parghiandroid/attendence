package com.example.atul.attendance.model

import org.json.JSONArray

data class BodyParam (var service : String, var user_identity : String)

data class BodyParamsenddata (var service : String, var data : List<Data>)

data class Data (var user_identity : String, var datetime : String)

data class GETUSERDATA(var service : String)