package com.example.atul.attendance.model

data class response(val success : String, val message : String, val data : ArrayList<data>)

data class data(val key : String, val value : String)

