package com.example.atul.attendance.database

object Table {

    //region For table name
    val TABLE_ATTENDENCE = "attandence"
    //endregion

    //region For column
    val ID = "id"
    val QR_RESULT = "qrresult"
    val DATE_TIME = "datetime"
    //endregion
}
