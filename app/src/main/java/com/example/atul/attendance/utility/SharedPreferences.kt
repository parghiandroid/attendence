package com.example.atul.attendance.utility

import android.content.Context
import com.example.atul.attendance.model.data
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.ArrayList

object SharedPreferences {

    private var preferences: android.content.SharedPreferences? = null
    private var editor: android.content.SharedPreferences.Editor? = null


    //region Shared Preference
    fun CreatePreference(context: Context, preferenceName: String) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        editor = preferences!!.edit()
        editor!!.apply()
    }
    //endregion

    fun SavePreference(preferenceKey: String, preferenceValue: String) {
        editor!!.putString(preferenceKey, preferenceValue)
        editor!!.apply()
    }

    fun GetPreference(context: Context, preferenceName: String, preferenceKey: String): String? {
        val text: String?
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        text = preferences!!.getString(preferenceKey, null)
        return text
    }

    fun GetuserdataPreference(context: Context, preferenceName: String, preferenceKey: String): ArrayList<data>? {
        val text: String?
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        text = preferences!!.getString(preferenceKey, null)
        //json = GlobalSharedPreferences.GetPreference(mcontext,"cartdata", "data");
        val type = object : TypeToken<ArrayList<data>>() {

        }.type
        val gson = Gson()
        return gson.fromJson<ArrayList<data>>(text, type)
    }

    fun ClearPreference(context: Context, preferenceName: String) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        editor = preferences!!.edit()
        editor!!.clear()
        editor!!.apply()
    }

    fun RemovePreference(context: Context, preferenceName: String, preferenceKey: String) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
        editor = preferences!!.edit()
        editor!!.remove(preferenceKey)
        editor!!.apply()
    }
}