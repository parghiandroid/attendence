package com.example.atul.attendance.activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.atul.attendance.R
import com.example.atul.attendance.database.DatabaseHandler
import com.example.atul.attendance.model.BodyParam
import com.example.atul.attendance.model.BodyParamsenddata
import com.example.atul.attendance.model.GETUSERDATA
import com.example.atul.attendance.model.response
import com.example.atul.attendance.reciever.NetworkChangeReceiver
import com.example.atul.attendance.utility.SendMail
import com.example.atul.attendance.utility.SharedPreferences
import com.example.atul.attendance.utility.StaticUtility
import com.example.atul.retrofit.api.ApiClient
import com.example.atul.retrofit.api.ApiInterface
import com.google.gson.Gson
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.InetAddress
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity(), ZXingScannerView.ResultHandler, NetworkChangeReceiver.ConnectivityReceiverListener {

    private var mScannerView: ZXingScannerView? = null

    val mContext = this@MainActivity

    lateinit var internetHandler: Handler

    lateinit var result: Result

    lateinit var pdailog: ProgressDialog

    lateinit var current_date_time : String

    var issave = false

    internal var gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        registerReceiver(NetworkChangeReceiver(),
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)
    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera() // Start camera on resume
        NetworkChangeReceiver.connectivityReceiverListener = this
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()           // Stop camera on pause
    }

    @SuppressLint("SimpleDateFormat")
    override fun handleResult(rawResult: Result) {
        result = rawResult
        // Do something with the result here
        // Log.v("tag", rawResult.getText()); // Prints scan results
        // Log.v("tag", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
        if(rawResult.barcodeFormat.toString().equals("QR_CODE", ignoreCase = true)) {
            val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            current_date_time = sdf.format(Date())
            pdailog = ProgressDialog(mContext)
            pdailog.setTitle("Loading...")
            pdailog.setMessage("Please Wait...")
            pdailog.setCancelable(false)
            pdailog.isIndeterminate
            pdailog.show()
            saveattandence()
            val isinternet = isinternetAvailable(internetHandler)
            isinternet.execute()
        }else{
            mScannerView?.resumeCameraPreview(mContext)
        }
    }
        class isinternetAvailable(internetHandler: Handler) : AsyncTask<Void, Void, Boolean>() {
             val handler  = internetHandler

            override fun doInBackground(vararg params: Void?): Boolean? {
                try {
                    val ipAddr = InetAddress.getByName("google.com")
                    //You can replace it with your name
                    return !ipAddr.equals("")

                } catch (e: Exception) {
                    return false
                }
            }
            override fun onPostExecute(isrunning: Boolean?) {
                super.onPostExecute(isrunning)

                if (handler != null) {
                    val msg = Message()
                    msg.obj = isrunning
                    handler.sendMessage(msg)
                }
            }
        }

    fun saveattandence(){
        internetHandler = android.os.Handler(Handler.Callback { msg ->
            try {
               if(msg.obj as Boolean){
                   val body = BodyParam("attendence", result.text)
                   val client = ApiClient.client.create(ApiInterface::class.java)

                   client.savetime(StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET,
                       StaticUtility.DEVICE_ID, StaticUtility.AUTH_TOKEN, body).enqueue(object : Callback<response> {
                       @SuppressLint("ShowToast")
                       override fun onResponse(call: Call<response>, response: retrofit2.Response<response>) {
                           var code = response.code()
                           var str = response.body()
                           Handler().postDelayed({
                               pdailog.dismiss()
                               mScannerView?.resumeCameraPreview(mContext)
                           },5000)
                           if(code == 200) {
                               var name = getusername()
                               Toast.makeText(mContext, "$name\nSuccessfully saved...!", Toast.LENGTH_LONG).show()
                               //Toast.makeText(mContext, name +"\n"+ str?.message, Toast.LENGTH_LONG).show()
                           }
                           else{
                               DatabaseHandler(mContext).Insertdata(result.text,current_date_time)
                               var name = getusername()
                               Toast.makeText(mContext, "$name\nSuccessfully saved...!", Toast.LENGTH_LONG).show()
                               StaticUtility.sendMail("Getting error in Save attandence in MainActivity.\n", response.toString())
                           }

                       }
                       override fun onFailure(call: Call<response>, t: Throwable) {
                           // Log error here since request failed
                           DatabaseHandler(mContext).Insertdata(result.text,current_date_time)
                           Handler().postDelayed({
                               pdailog.dismiss()
                               mScannerView?.resumeCameraPreview(mContext)
                           },5000)
                           var name = getusername()
                           Toast.makeText(mContext, "$name\nSuccessfully saved...!", Toast.LENGTH_LONG).show()
                           StaticUtility.sendMail("Getting error in Save attandence in MainActivity.\n", t.toString())
                       }
                   })
               }else{
                   DatabaseHandler(mContext).Insertdata(result.text,current_date_time)
                   var name = getusername()
                   Toast.makeText(mContext, "$name\nSuccessfully saved...!", Toast.LENGTH_LONG).show()
                   Handler().postDelayed({
                       pdailog.dismiss()
                       mScannerView?.resumeCameraPreview(mContext)
                   },5000)
                   //Toast.makeText(mContext, "nathi chaltu", Toast.LENGTH_LONG).show()
               }
            } catch (e: JSONException) {

            }
            true
        })
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if(isConnected) {
            if(!issave) {
                senddata()
                GetUserData()
            }
        }
        //Toast.makeText(mContext, isConnected.toString(), Toast.LENGTH_LONG).show()
    }

    fun senddata(){
        issave = true
        val arrayattandence  = DatabaseHandler(mContext).attendence
        if(arrayattandence.isNotEmpty()) {
            val body = BodyParamsenddata("bulk_attendence", arrayattandence)
            val client = ApiClient.client.create(ApiInterface::class.java)

            client.senddata(
                StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET,
                StaticUtility.DEVICE_ID, StaticUtility.AUTH_TOKEN, body
            ).enqueue(object : Callback<response> {
                @SuppressLint("ShowToast")
                override fun onResponse(call: Call<response>, response: Response<response>) {
                    //MainActivity().pdailog.dismiss()
                    issave = false
                    if(response.code() == 200) {
                        var success = response.body()!!.success
                        if (success.equals("true", ignoreCase = true)) {
                            DatabaseHandler(mContext).Deletedata()
                        }else{
                            StaticUtility.sendMail("Getting error in Senddata to server in MainActivity.\n",response.toString())
                        }
                    }else{
                        StaticUtility.sendMail("Getting error in Senddata to server in MainActivity.\n",response.toString())
                    }
                }
                override fun onFailure(call: Call<response>, t: Throwable) {
                    //MainActivity().pdailog.dismiss()
                    //Log error here since request failed
                    //Creating SendMail object
                    issave = false
                    StaticUtility.sendMail("Getting error in Senddata to server in MainActivity.\n",t.toString())
                }
            })
        }
    }

    fun GetUserData(){
            val body = GETUSERDATA("user_data")
            val client = ApiClient.client.create(ApiInterface::class.java)

            client.getuserdata(
                StaticUtility.CONTENT_TYPE, StaticUtility.APP_ID, StaticUtility.APP_SECRET,
                StaticUtility.DEVICE_ID, StaticUtility.AUTH_TOKEN, body
            ).enqueue(object : Callback<response> {
                @SuppressLint("ShowToast")
                override fun onResponse(call: Call<response>, response: Response<response>) {
                    //MainActivity().pdailog.dismiss()
                    if(response.code() == 200) {
                        var success = response.body()!!.success
                        val data = response.body()!!.data
                        if (success.equals("true", ignoreCase = true)) {
                            val json = gson.toJson(data)
                            SharedPreferences.CreatePreference(mContext,StaticUtility.USER_DATA)
                            SharedPreferences.SavePreference(StaticUtility.USER_DATA,json)
                            //Toast.makeText(mContext, data.toString(), Toast.LENGTH_LONG).show()
                        }else{
                            StaticUtility.sendMail("Getting error in getUserdata from server in MainActivity.\n",response.body().toString())
                        }
                    }else{
                        StaticUtility.sendMail("Getting error in getUserdata from server in MainActivity.\n",response.body().toString())
                    }
                }
                override fun onFailure(call: Call<response>, t: Throwable) {
                    //MainActivity().pdailog.dismiss()
                    //Log error here since request failed
                    //Creating SendMail object
                    StaticUtility.sendMail("Getting error in getUserdata from server in MainActivity.\n",t.toString())
                }
            })
    }

    fun getusername(): String {
        var name : String = ""
        if(SharedPreferences.GetuserdataPreference(mContext, StaticUtility.USER_DATA, StaticUtility.USER_DATA) != null) {
            val data = SharedPreferences.GetuserdataPreference(
                mContext, StaticUtility.USER_DATA, StaticUtility.USER_DATA
            )
            for (i in data!!) {
                if (i.key.equals(result.text, ignoreCase = true)) {
                    name = i.value
                }
            }
        }
        return name
    }

}
